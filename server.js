const express = require('express');
const cors = require('cors');
const mysql      = require('mysql');

const products = require('./app/newses');
const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database : 'newses'
});

connection.connect((err) => {
  app.use('/', products(connection));

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});