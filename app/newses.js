const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = (db) => {
    router.get('/:type', (req, res) => {
        if (req.query.news_id) {
            db.query('SELECT * FROM `comments` WHERE idNews = ' + req.query.news_id + ';', function (error, results) {
                if (error) res.status(400).send(error.sqlMessage);

                res.send(results);
            });
        } else {
            db.query('SELECT * FROM `' + req.params.type + '`', function (error, results) {
                if (error) res.status(400).send(error.sqlMessage);

                res.send(results);
            });
        }

    });

    router.post('/news', upload.single('image'), (req, res) => {
        const item = req.body;

        if (req.file) {
            item.image = req.file.filename;
        } else {
            item.image = null;
        }

        db.query(
            'INSERT INTO `news` (`header`, `body`, `image`) ' +
            'VALUES (?, ?, ?)',
            [item.header, item.body, item.image],
            (error, results) => {
                if (error) res.status(400).send(error.sqlMessage);

                item.id = results.insertId;
                res.send(item);
            }
        );
    });

    router.post('/comments', (req, res) => {
        const comment = req.body;

        db.query(
            'INSERT INTO `comments` (`idNews`, `author`, `comment`) ' +
            'VALUES (?, ?, ?)',
            [comment.idNews, comment.author, comment.comment],
            (error, results) => {
                if (error) res.status(400).send(error.sqlMessage);

                comment.id = results.insertId;
                res.send(comment);
            }
        );
    });

    // Product get by ID
    router.get('/:type/:id', (req, res) => {
        db.query('SELECT * FROM `' + req.params.type + '` WHERE id = ' + req.params.id + ';', function (error, results) {
            if (error) res.status(400).send(error.sqlMessage);

            res.send(results[0]);
        });

        router.delete('/:type/:id', (req, res) => {
            db.query('DELETE FROM `' + req.params.type + '` WHERE id = ' + req.params.id + ';', (error, result) => {
                if (error) res.status(400).send(error.sqlMessage);

                console.log('result_________delete', result);
            })
        });

    });
    return router;
};

module.exports = createRouter;